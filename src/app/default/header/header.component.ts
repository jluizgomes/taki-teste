import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isMenu = false;
  constructor() { }

  ngOnInit() {
    
  }

  showMenu() {
    this.isMenu = true;
  }

  hideMenu() {
    this.isMenu = false;
  }
}
