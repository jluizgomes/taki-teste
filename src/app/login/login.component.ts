import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { default as swal } from 'sweetalert2';

import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginUser: FormGroup;

  login = {
    Email: "",
    Password: ""
  };

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _fb: FormBuilder
  ) {
    this.loginUser = this._fb.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          )
        ])
      ],
      senha: ["", Validators.required],
    });
  }

  ngOnInit() {
  }

  getLogin() {
    this.login.Email = this.loginUser.get("email").value;
    this.login.Password = this.loginUser.get("senha").value;

    if(this.login.Email === "taki@taki.com.vc" || this.login.Password === "taki2018") {
      this._router.navigate(['home']);
    } else {
      swal({
        type: 'error',
        title: 'Oops...',
        text: 'E-mail e/ou Senha inválidos, tente novamente.',
      })
    }
  }
}
